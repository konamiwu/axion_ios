//
//  ColumnView.swift
//  Axiom
//
//  Created by Konami on 2024/5/20.
//

import Foundation
import UIKit

class ColumnView: UIView {
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var button: UIButton!
    @IBOutlet var label: UILabel!
    private var isHighlight = false
    private var highlightColor = UIColor(red: 0.12, green: 0.82, blue: 0.84, alpha: 1.00)
    var row: Int = 0 {
        didSet {
            addCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        label.text = String.confirm
        label.translatesAutoresizingMaskIntoConstraints = false
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.cornerRadius = 4
        
        layer.cornerRadius = 4
        layer.masksToBounds = false
    }
    
    private func addCell() {
        stackView.arrangedSubviews.forEach {
            $0.removeFromSuperview()
        }
        
        (0..<row).forEach { _ in
            let view = UIView()
            view.backgroundColor = UIColor(red: CGFloat.random(in: 0...1),
                                           green: CGFloat.random(in: 0...1),
                                           blue: CGFloat.random(in: 0...1),
                                           alpha: 1)
            stackView.addArrangedSubview(view)
        }
    }
    
    func highlight() {
        layer.borderWidth = 4
        layer.borderColor = highlightColor.cgColor
        button.backgroundColor = highlightColor
        button.layer.borderColor = highlightColor.cgColor
        label.textColor = .white
    }
    
    func resetHighlight() {
        layer.borderWidth = 0
        layer.borderColor = nil
        button.backgroundColor = nil
        button.layer.borderColor = UIColor.lightGray.cgColor
        label.textColor = .lightGray
    }
}
