//
//  ViewController.swift
//  Axiom
//
//  Created by Konami on 2024/5/17.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet private var containerView: UIView!
    @IBOutlet private var stackView: UIStackView!
    @IBOutlet private var rowTextField: UITextField!
    @IBOutlet private var columnTextField: UITextField!
    
    private var row: Int = 0
    private var column: Int = 0
    private var previousHighlight: ColumnView?
    private var borderWidth = 8
    private var label = UILabel()
    private var timer: Timer?
    private var timerPeriod: TimeInterval = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        label.text = "random"
        label.textColor = .black
        label.font = label.font.withSize(16)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
    }

    @IBAction func addAction() {
        guard let rowText = rowTextField.text,
              let row = Int(rowText),
              let columnText = columnTextField.text,
              let column = Int(columnText) else {
            return
        }
        
        stackView.arrangedSubviews.forEach {
            $0.removeFromSuperview()
        }
        
        self.row = row
        self.column = column
        
        (0..<column).forEach { _ in
            guard let columnView = (UINib(nibName: "ColumnView", bundle: .main).instantiate(withOwner: nil)[0]) as? ColumnView else {
                return
            }
            columnView.row = row
            columnView.button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            stackView.addArrangedSubview(columnView)
        }
        
        startTimer()
    }
    
    private func startTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: timerPeriod, repeats: true) { [weak self] timer in
            guard let self = self else {
                return
            }
            self.previousHighlight?.resetHighlight()
            let randomRow = Int.random(in: 0..<self.row)
            let randomColumn = Int.random(in: 0..<self.column)
            
            guard let columView = self.stackView.arrangedSubviews[randomColumn] as? ColumnView else {
                return
            }
            let view = columView.stackView.arrangedSubviews[randomRow]
            view.addSubview(label)
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            label.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
            label.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
            columView.highlight()
            self.previousHighlight = columView
        }
        timer?.fire()
    }
    
    @objc private func buttonAction() {
        self.previousHighlight?.resetHighlight()
        label.removeFromSuperview()
    }
}

extension ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            return true
        }
        return Int(string) != nil
    }
}

