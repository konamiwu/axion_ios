//
//  String.swift
//  Axiom
//
//  Created by Konami on 2024/5/20.
//

import Foundation

extension String {
    static var confirm: String {
        return NSLocalizedString("confirm", comment: "")
    }
}
